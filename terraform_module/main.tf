resource "helm_release" "arangodb" {
  chart           = "arangodb-instance"
  repository      = "https://gitlab.com/api/v4/projects/36276352/packages/helm/stable"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = var.values
}
